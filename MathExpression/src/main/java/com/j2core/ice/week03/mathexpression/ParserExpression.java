package com.j2core.ice.week03.mathexpression;

import java.util.LinkedList;

/**
 * Created by Serge Kotvitsky on 6/16/16.
 */
public class ParserExpression {

    public static final char OP_BRACKET = '(';
    public static final char CL_BRACKET = ')';
    public static final char ADD = '+';
    public static final char SUB = '-';
    public static final char MUL = '*';
    public static final char DIV = '/';
    public static final char FL_POINT = '.';

    /**
     * This method converts the expression into a proper mathematical notation.
     * Example:
     * expression: +15+((+150--12.5*4))/(+20*(+15+(-3-+2))-+1++1)++5+-5-15
     * transformed expression: 15+((150-(-12.5)*4))/(20*(15+(-3-2))-1+1)+5+(-5)-15
     *
     * @param expression Expression to analyze type of String
     * @return transformed expression
     */
    public static String convertMathExpression(String expression) {

        StringBuilder exp = new StringBuilder(expression);

        for (int i = 0; i < exp.length() - 2; i++) {

            if (i == 0 && exp.charAt(i) == ADD)
                exp = exp.deleteCharAt(i);

            else if (exp.charAt(i) == OP_BRACKET && exp.charAt(i + 1) == ADD && Character.isDigit(exp.charAt(i + 2)))
                exp = exp.deleteCharAt(i + 1);

            else if ((exp.charAt(i) == ADD || exp.charAt(i) == SUB) && (exp.charAt(i + 1) == ADD || exp.charAt(i + 1) == SUB)
                    && Character.isDigit(exp.charAt(i + 2))) {

                int j = i;

                while (i < exp.length() - 2 && (Character.isDigit(exp.charAt(i + 2)) || exp.charAt(i + 2) == FL_POINT))
                    i++;

                if (exp.charAt(j + 1) == ADD)
                    exp = exp.replace(j + 1, i + 2, exp.substring(j + 2, i + 2));
                else
                    exp = exp.replace(j + 1, i + 2, "(" + exp.charAt(j + 1) + exp.substring(j + 2, i + 2) + ")");
            }
        }
        return String.valueOf(exp);
    }

    /**
     * This method for parse and calculation of the mathematical expression
     *
     * @param exp Expression to parse
     * @return The result of calculation
     * @throws java.lang.NumberFormatException If multiple points in the numbers
     */
    public static double parsMathExpression(String exp) {

        LinkedList<Double> arg = new LinkedList<>();
        LinkedList<Character> oper = new LinkedList<>();

        try {
            for (int i = 0; i < exp.length(); i++) {

                if (exp.charAt(i) == OP_BRACKET) {
                    oper.add(OP_BRACKET);

                } else if (exp.charAt(i) == CL_BRACKET) {
                    while (oper.getLast() != OP_BRACKET)
                        arg.add(MathOperations.calcExpression(arg.removeLast(), arg.removeLast(), oper.removeLast()));
                    oper.removeLast();

                } else if (ValidationUtils.checkNegativeNumber(exp, i) || Character.isDigit(exp.charAt(i)) || exp.charAt(i) == FL_POINT) {
                    boolean negative = false;
                    if (exp.charAt(i) == SUB) {
                        negative = true;
                        i++;
                    }
                    int j = i;
                    while (i < exp.length() && (Character.isDigit(exp.charAt(i)) || exp.charAt(i) == FL_POINT))
                        i++;
                    if (negative)
                        arg.add(-Double.parseDouble(exp.substring(j, i)));
                    else
                        arg.add(Double.parseDouble(exp.substring(j, i)));
                    --i;

                } else {
                    while (!oper.isEmpty() && oper.getLast() != OP_BRACKET && oper.getLast() != CL_BRACKET
                            && MathOperations.getMathOperation(oper.getLast()).getPriority()
                            >= MathOperations.getMathOperation(exp.charAt(i)).getPriority()) {

                        arg.add(MathOperations.calcExpression(arg.removeLast(), arg.removeLast(), oper.removeLast()));
                    }
                    oper.add(exp.charAt(i));
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Incorrect mathematical expression");
        }
        while (!oper.isEmpty())
            arg.add(MathOperations.calcExpression(arg.removeLast(), arg.removeLast(), oper.removeLast()));
        return arg.getLast();
    }
}