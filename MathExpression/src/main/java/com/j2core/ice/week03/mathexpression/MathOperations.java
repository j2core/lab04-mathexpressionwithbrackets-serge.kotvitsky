package com.j2core.ice.week03.mathexpression;

/**
 * Created by Serge Kotvitsky on 6/25/16.
 */
public enum MathOperations {

    ADD('+', 1),
    SUB('-', 1),
    MUL('*', 2),
    DIV('/', 2);


    private int priority;
    private char sign;

    MathOperations(char sign, int priority) {
        this.sign = sign;
        this.priority = priority;
    }

    public char getSignOper() {
        return sign;
    }
    public int getPriority() {
        return priority;
    }

    /**
     * This method is intended in order to obtain the mathematical operation on its symbolic representation.
     *
     * @param symbol symbol type of char
     * @return  mathematical operation
     * @throws IllegalArgumentException If the symbol is not a mathematical operation.
     */
    public static MathOperations getMathOperation(char symbol) {

        for (MathOperations value : values()) {
            if (value.sign == symbol)
                return value;
        }
        throw new IllegalArgumentException("Symbol " + "'" + symbol + "'" + " it's not a mathematical operation");
    }

    /**
     * This method checks whether the passed parameter mathematical operation.
     *
     * @param symbol verifiable symbol
     * @return true if the checked symbol is a mathematical operation otherwise return false
     */
    public static boolean validMathOperation(char symbol) {

        try {
            getMathOperation(symbol);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * This is a method for calculating mathematical expressions
     *
     * @param op1  first operand type of double
     * @param op2  second operand type of double
     * @param oper mathematical operation type of char
     * @return calculation result type of double
     * @throws IllegalArgumentException If there is a division by zero  or unsupported mathematical operation
     */
    public static double calcExpression(double op1, double op2, char oper) {

        switch (getMathOperation(oper)) {
            case ADD:
                return op2 + op1;
            case SUB:
                return op2 - op1;
            case MUL:
                return op2 * op1;
            case DIV:
                if (op1 == 0)
                    throw new IllegalArgumentException("Division by zero");
                return op2 / op1;
            default:
                throw new IllegalArgumentException("Unsupported mathematical operation" + oper);
        }
    }
}

