package com.j2core.ice.week03.mathexpression;

import static com.j2core.ice.week03.mathexpression.ParserExpression.*;

/**
 * Created by Serge Kotvitsky on 6/16/16.
 */
class ValidationUtils {

    /**
     * This method checks whether a negative number
     *
     * @param exp   expression to analyze type of String
     * @param index Index of the character in the string
     * @return true if the number is negative, otherwise returns false
     */
    public static boolean checkNegativeNumber(String exp, int index) {

        return (exp.charAt(index) == SUB && (index == 0 || exp.charAt(index - 1) == OP_BRACKET)
                && exp.length() > 1 && index < exp.length() - 1 && Character.isDigit(exp.charAt(index + 1)));
    }

    /**
     * This method checks the number of opening and closing brackets
     *
     * @param expression expression to analyze type of String
     * @return false if the number of opening and closing brackets unequal
     * or they are located is not a in proper manner otherwise return true
     */
    public static boolean validBracketsBalanced(String expression) {

        int balance = 0;

        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == OP_BRACKET) {
                balance++;
            } else if (expression.charAt(i) == CL_BRACKET) {
                balance--;
            }
            if (balance < 0) {
                System.out.println("Opening and closing brackets are located is not a in  proper manner");
                return false;
            }
        }
        if (balance != 0) {
            System.out.println("The number of opening and closing brackets unequal");
            return false;
        }
        return true;
    }

    /**
     * This method validates the mathematical expression
     * and the correct position of the operation and the operands in this expression.
     *
     * @param exp expression to analyze type of String
     * @return true if the mathematical expression does not contain invalid characters
     * and positions of operators and operands are positioned correctly otherwise return false
     */
    public static boolean validMathExpression(String exp) {

        for (int i = 0; i < exp.length(); i++) {

            switch (exp.charAt(i)) {

                case OP_BRACKET:
                    if ((i == 0 && exp.length() > 1 && i < exp.length() - 1 && (exp.charAt(i + 1) == OP_BRACKET
                            || exp.charAt(i + 1) == SUB || exp.charAt(i + 1) == ADD || Character.isDigit(exp.charAt(i + 1))))
                            || (i > 0 && exp.length() > 1 && i < exp.length() - 1 && (exp.charAt(i - 1) == OP_BRACKET
                            || MathOperations.validMathOperation(exp.charAt(i - 1))))
                            && (exp.charAt(i + 1) == OP_BRACKET || exp.charAt(i + 1) == SUB
                            || exp.charAt(i + 1) == ADD || Character.isDigit(exp.charAt(i + 1)))) break;
                    else return false;
                case ADD: case SUB:
                case MUL: case DIV:
                    if ((i > 0 && i < exp.length() - 1 && (exp.charAt(i - 1) == CL_BRACKET && exp.charAt(i + 1) == OP_BRACKET
                            || Character.isDigit(exp.charAt(i - 1)) && Character.isDigit(exp.charAt(i + 1))
                            || exp.charAt(i - 1) == CL_BRACKET && Character.isDigit(exp.charAt(i + 1))
                            || Character.isDigit(exp.charAt(i - 1)) && exp.charAt(i + 1) == OP_BRACKET))
                            || checkNegativeNumber(exp, i)) break;
                    else return false;
                case '0': case '1': case '2':
                case '3': case '4': case '5':
                case '6': case '7': case '8':
                case '9': case FL_POINT:
                    if (i == 0 && Character.isDigit(exp.charAt(i))
                            || i > 0 && Character.isDigit(exp.charAt(i)) && exp.charAt(i - 1) != CL_BRACKET
                            || i > 0 && exp.length() > 1 && (exp.charAt(i) == FL_POINT && i < exp.length() - 1
                            && Character.isDigit(exp.charAt(i - 1)) && Character.isDigit(exp.charAt(i + 1)))) break;
                    else return false;
                case CL_BRACKET: break;
                default: return false;
            }
        }
        return true;
    }
}