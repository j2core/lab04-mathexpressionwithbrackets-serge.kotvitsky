package com.j2core.ice.week03.mathexpression;

/**
 * Created by Serge Kotvitsky on 6/16/16.
 */
public class MainMathExpression {

    public static void main(String[] args) {

        for (int i = 0; i < args.length; i++) {

            args[i] = ParserExpression.convertMathExpression(args[i]);

            if (!ValidationUtils.validBracketsBalanced(args[i]) || !ValidationUtils.validMathExpression(args[i]))
                System.err.println("Expression " + i + " : " + args[i] + " incorrect");
            else
                System.out.println("Expression " + i + " : " + args[i] + " = " + ParserExpression.parsMathExpression(args[i]));
        }
    }
}
